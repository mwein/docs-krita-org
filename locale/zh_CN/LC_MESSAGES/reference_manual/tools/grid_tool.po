msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-16 17:04\n"
"Last-Translator: Guo Yunhe (guoyunhe)\n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/www/"
"docs_krita_org_reference_manual___tools___grid_tool.pot\n"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"

#: ../../<rst_epilog>:48
msgid ""
".. image:: images/icons/grid_tool.svg\n"
"   :alt: toolgrid"
msgstr ""
".. image:: images/icons/grid_tool.svg\n"
"   :alt: toolgrid"

#: ../../reference_manual/tools/grid_tool.rst:1
msgid "Krita's grid tool reference."
msgstr ""

#: ../../reference_manual/tools/grid_tool.rst:11
msgid "Tools"
msgstr ""

#: ../../reference_manual/tools/grid_tool.rst:11
msgid "Grid"
msgstr ""

#: ../../reference_manual/tools/grid_tool.rst:16
msgid "Grid Tool"
msgstr "网格工具"

#: ../../reference_manual/tools/grid_tool.rst:18
msgid "|toolgrid|"
msgstr ""

#: ../../reference_manual/tools/grid_tool.rst:22
msgid "Deprecated in 3.0, use the :ref:`grids_and_guides_docker` instead."
msgstr ""

#: ../../reference_manual/tools/grid_tool.rst:24
msgid ""
"When you click on the edit grid tool, you'll get a message saying that to "
"activate the grid you must press the :kbd:`Enter` key. Press the :kbd:"
"`Enter` key to make the grid visible. Now you must have noticed the tool "
"icon for your pointer has changed to icon similar to move tool."
msgstr ""

#: ../../reference_manual/tools/grid_tool.rst:27
msgid ""
"To change the spacing of the grid, press and hold the :kbd:`Ctrl` key and "
"then the |mouseleft| :kbd:`+ drag` shortcut on the canvas. In order to move "
"the grid you have to press the :kbd:`Alt` key and then the |mouseleft| :kbd:`"
"+ drag` shortcut."
msgstr ""
