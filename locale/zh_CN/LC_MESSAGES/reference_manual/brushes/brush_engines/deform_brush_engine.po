msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-15 03:16+0200\n"
"PO-Revision-Date: 2019-08-16 17:04\n"
"Last-Translator: Guo Yunhe (guoyunhe)\n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/www/"
"docs_krita_org_reference_manual___brushes___brush_engines___deform_brush_engine."
"pot\n"

#: ../../<generated>:1
msgid "Use Undeformed Image"
msgstr "采样扭曲前图像"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:1
msgid "The Deform Brush Engine manual page."
msgstr "介绍 Krita 的扭曲笔刷引擎。"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:11
#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:16
msgid "Deform Brush Engine"
msgstr "扭曲笔刷引擎"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:11
msgid "Brush Engine"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:11
msgid "Deform"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:11
msgid "Liquify"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:19
msgid ".. image:: images/icons/deformbrush.svg"
msgstr ".. image:: images/icons/deformbrush.svg"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:20
msgid ""
"The Deform Brush is a brush that allows you to pull and push pixels around. "
"It's quite similar to the :ref:`liquify_mode`, but where liquify has higher "
"quality, the deform brush has the speed."
msgstr ""
"这是一个可以将像素进行推挤和拉扯的笔刷引擎。它和变形工具的 :ref:"
"`liquify_mode` 模式类似，但液化变形的品质更高，扭曲笔刷引擎的处理速度更快。"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:24
msgid "Options"
msgstr "可用选项"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:26
msgid ":ref:`option_brush_tip`"
msgstr ":ref:`option_brush_tip`"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:27
msgid ":ref:`option_deform`"
msgstr ":ref:`option_deform`"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:28
msgid ":ref:`blending_modes`"
msgstr ":ref:`blending_modes`"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:29
msgid ":ref:`option_opacity_n_flow`"
msgstr ":ref:`option_opacity_n_flow`"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:30
msgid ":ref:`option_size`"
msgstr ":ref:`option_size`"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:31
msgid ":ref:`option_rotation`"
msgstr ":ref:`option_rotation`"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:32
msgid ":ref:`option_airbrush`"
msgstr ":ref:`option_airbrush`"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:38
msgid "Deform Options"
msgstr "扭曲选项"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:42
msgid ".. image:: images/brushes/Krita_deform_brush_examples.png"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:42
msgid ""
"1: undeformed, 2: Move, 3: Grow, 4: Shrink, 5: Swirl Counter Clock Wise, 6: "
"Swirl Clockwise, 7: Lens Zoom In, 8: Lens Zoom Out"
msgstr ""
"1) 扭曲前；2) 移动；3) 扩张；4) 收缩；5) 漩涡 (顺时针)；6) 漩涡 (逆时针)；7) "
"镜头缩小；8) 镜头放大"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:44
msgid "These decide what strangeness may happen underneath your brush cursor."
msgstr "从这些选项里面选择一种扭曲图像形状的特效。"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:46
msgid "Grow"
msgstr "扩张"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:47
msgid "This bubbles up the area underneath the brush-cursor."
msgstr "此选项会把笔刷光标下方的区域像吹泡泡一样进行扩张。"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:48
msgid "Shrink"
msgstr "收缩"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:49
msgid "This pinches the Area underneath the brush-cursor."
msgstr "此选项会把笔刷光标下方的区域向内收缩。"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:50
msgid "Swirl Counter Clock Wise"
msgstr "漩涡 (逆时针)"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:51
msgid "Swirls the area counter clock wise."
msgstr "此选项会把笔刷光标下方的区域逆时针旋转。"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:52
msgid "Swirl Clock Wise"
msgstr "漩涡 (顺时针)"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:53
msgid "Swirls the area clockwise."
msgstr "此选项会把笔刷光标下方的区域顺时针旋转。"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:54
msgid "Move"
msgstr "移动"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:55
msgid "Nudges the area to the painting direction."
msgstr "此选项会把笔刷光标下方的区域向绘制方向推挤。"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:56
msgid "Color Deformation"
msgstr "颜色扭曲"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:57
msgid "This seems to randomly rearrange the pixels underneath the brush."
msgstr "此选项会把笔刷光标下方的区域的颜色进行随机抖动。"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:58
msgid "Lens Zoom In"
msgstr "镜头放大"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:59
msgid "Literally paints a enlarged version of the area."
msgstr "此选项采样位于笔刷光标下方的区域并生成一个放大版本。"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:61
msgid "Lens Zoom Out"
msgstr "镜头缩小"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:61
msgid "Paints a minimized version of the area."
msgstr "此选项采样位于笔刷光标下方的区域并生成一个缩小版本。"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:65
msgid ".. image:: images/brushes/Krita_deform_brush_colordeform.png"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:65
msgid "Showing color deform."
msgstr "上图展示了颜色扭曲的效果。"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:68
msgid "Deform Amount"
msgstr "扭曲量"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:68
msgid "Defines the strength of the deformation."
msgstr "此项定义扭曲的效果强度。"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:72
msgid ".. image:: images/brushes/Krita_deform_brush_bilinear.png"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:72
#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:74
msgid "Bilinear Interpolation"
msgstr "上图展示了双线性插值选项的效果。"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:75
msgid "Smoothens the result. This causes calculation errors in 16bit."
msgstr ""
"此选项会将扭曲的结果用双线性插值算法进行平滑。但它在 16 位深度下面会造成计算"
"错误。"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:77
msgid "Use Counter"
msgstr "使用计数器"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:77
msgid "Slows down the deformation subtlety."
msgstr "此选项会略微减缓扭曲的速度。"

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:81
msgid ".. image:: images/brushes/Krita_deform_brush_useundeformed.png"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:81
msgid "Without 'use undeformed' to the left and with to the right."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/deform_brush_engine.rst:84
msgid ""
"Samples from the previous version of the image instead of the current. This "
"works better with some deform options than others. Move for example seems to "
"almost stop working, but it works really well with Grow."
msgstr ""
"此选项会让持续的扭曲操作对扭曲前图像进行采样，而不是对当前状态进行采样。有些"
"扭曲操作配合此选项使用时效果更好，例如扩张。但也有些扭曲操作水土不服，例如移"
"动就几乎完全失去效果。"
