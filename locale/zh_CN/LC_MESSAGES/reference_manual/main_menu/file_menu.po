msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-15 03:16+0200\n"
"PO-Revision-Date: 2019-08-16 17:04\n"
"Last-Translator: Guo Yunhe (guoyunhe)\n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/www/"
"docs_krita_org_reference_manual___main_menu___file_menu.pot\n"

#: ../../reference_manual/main_menu/file_menu.rst:1
msgid "The file menu in Krita."
msgstr ""

#: ../../reference_manual/main_menu/file_menu.rst:12
#: ../../reference_manual/main_menu/file_menu.rst:23
msgid "Open"
msgstr "打开"

#: ../../reference_manual/main_menu/file_menu.rst:12
#: ../../reference_manual/main_menu/file_menu.rst:29
msgid "Save"
msgstr "保存"

#: ../../reference_manual/main_menu/file_menu.rst:12
#: ../../reference_manual/main_menu/file_menu.rst:38
msgid "Export"
msgstr "导出"

#: ../../reference_manual/main_menu/file_menu.rst:12
#: ../../reference_manual/main_menu/file_menu.rst:62
msgid "Close"
msgstr "关闭"

#: ../../reference_manual/main_menu/file_menu.rst:12
msgid "New File"
msgstr ""

#: ../../reference_manual/main_menu/file_menu.rst:12
msgid "Import"
msgstr ""

#: ../../reference_manual/main_menu/file_menu.rst:12
msgid "Template"
msgstr ""

#: ../../reference_manual/main_menu/file_menu.rst:17
msgid "File Menu"
msgstr "文件菜单"

#: ../../reference_manual/main_menu/file_menu.rst:20
msgid "New"
msgstr "新建"

#: ../../reference_manual/main_menu/file_menu.rst:22
msgid "Make a new file."
msgstr ""

#: ../../reference_manual/main_menu/file_menu.rst:25
msgid "Open a previously created file."
msgstr ""

#: ../../reference_manual/main_menu/file_menu.rst:26
msgid "Open Recent"
msgstr "打开最近一项"

#: ../../reference_manual/main_menu/file_menu.rst:28
msgid "Open the recently opened document."
msgstr ""

#: ../../reference_manual/main_menu/file_menu.rst:31
msgid ""
"File formats that Krita can save to. These formats can later be opened back "
"up in Krita."
msgstr ""

#: ../../reference_manual/main_menu/file_menu.rst:32
msgid "Save As"
msgstr "另存为"

#: ../../reference_manual/main_menu/file_menu.rst:34
msgid "Save as a new file."
msgstr ""

#: ../../reference_manual/main_menu/file_menu.rst:35
msgid "Open Existing Document As New document"
msgstr ""

#: ../../reference_manual/main_menu/file_menu.rst:37
msgid "Similar to import in other programs."
msgstr ""

#: ../../reference_manual/main_menu/file_menu.rst:40
msgid ""
"Additional formats that can be saved. Some of these formats may not be later "
"imported or opened by Krita."
msgstr ""

#: ../../reference_manual/main_menu/file_menu.rst:41
msgid "Import Animation Frames"
msgstr ""

#: ../../reference_manual/main_menu/file_menu.rst:43
msgid "Import frames for animation."
msgstr ""

#: ../../reference_manual/main_menu/file_menu.rst:44
msgid "Render Animation"
msgstr "渲染动画"

#: ../../reference_manual/main_menu/file_menu.rst:46
msgid ""
"Render an animation with FFmpeg. This is explained on the :ref:"
"`render_animation` page."
msgstr ""

#: ../../reference_manual/main_menu/file_menu.rst:47
msgid "Save incremental version"
msgstr ""

#: ../../reference_manual/main_menu/file_menu.rst:49
msgid "Save as a new version of the same file with a number attached."
msgstr ""

#: ../../reference_manual/main_menu/file_menu.rst:50
msgid "Save incremental Backup"
msgstr ""

#: ../../reference_manual/main_menu/file_menu.rst:52
msgid ""
"Copies and renames the last saved version of your file to a back-up file and "
"saves your document under the original name."
msgstr ""

#: ../../reference_manual/main_menu/file_menu.rst:53
msgid "Create Template from image"
msgstr ""

#: ../../reference_manual/main_menu/file_menu.rst:55
msgid ""
"The \\*.kra file will be saved into the template folder for future use. All "
"your layers and guides will be saved along!"
msgstr ""

#: ../../reference_manual/main_menu/file_menu.rst:56
msgid "Create Copy From Current Image"
msgstr "从当前图像创建副本"

#: ../../reference_manual/main_menu/file_menu.rst:58
msgid ""
"Makes a new document from the current image, so you can easily reiterate on "
"a single image. Useful for areas where the template system is too powerful."
msgstr ""

#: ../../reference_manual/main_menu/file_menu.rst:59
msgid "Document Information"
msgstr "文档信息"

#: ../../reference_manual/main_menu/file_menu.rst:61
msgid ""
"Look at the document information. Contains all sorts of interesting "
"information about image, such as technical information or metadata."
msgstr ""

#: ../../reference_manual/main_menu/file_menu.rst:64
msgid "Close the view or document."
msgstr ""

#: ../../reference_manual/main_menu/file_menu.rst:65
msgid "Close All"
msgstr "关闭全部"

#: ../../reference_manual/main_menu/file_menu.rst:67
msgid "Close all views and documents."
msgstr ""

#: ../../reference_manual/main_menu/file_menu.rst:68
msgid "Quit"
msgstr "退出"

#: ../../reference_manual/main_menu/file_menu.rst:70
msgid "Close Krita."
msgstr ""
