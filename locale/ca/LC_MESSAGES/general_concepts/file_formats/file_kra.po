# Translation of docs_krita_org_general_concepts___file_formats___file_kra.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: general_concepts\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-07-19 03:24+0200\n"
"PO-Revision-Date: 2019-07-19 13:42+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.07.70\n"

#: ../../general_concepts/file_formats/file_kra.rst:1
msgid "The Krita Raster Archive file format."
msgstr "El format de fitxer Arxiu rasteritzat del Krita."

#: ../../general_concepts/file_formats/file_kra.rst:10
msgid "*.kra"
msgstr "*.kra"

#: ../../general_concepts/file_formats/file_kra.rst:10
msgid "KRA"
msgstr "KRA"

#: ../../general_concepts/file_formats/file_kra.rst:10
msgid "Krita Archive"
msgstr "Arxiu del Krita"

#: ../../general_concepts/file_formats/file_kra.rst:15
msgid "\\*.kra"
msgstr "\\*.kra"

# skip-rule: t-acc_obe
#: ../../general_concepts/file_formats/file_kra.rst:17
msgid ""
"``.kra`` is Krita's internal file-format, which means that it is the file "
"format that saves all of the features Krita can handle. It's construction is "
"vaguely based on the open document standard, which means that you can rename "
"your ``.kra`` file to a ``.zip`` file and open it up to look at the insides."
msgstr ""
"El ``.kra`` és el format de fitxer intern del Krita, el qual vol dir que és "
"el format de fitxer en el que es desen totes les característiques que pot "
"gestionar el Krita. La seva construcció es basa vagament en l'estàndard de "
"document obert, el qual vol dir que podreu canviar l'extensió del vostre "
"fitxer ``.kra`` a un fitxer ``.zip`` i obrir-lo per a mirar l'interior."

#: ../../general_concepts/file_formats/file_kra.rst:19
msgid ""
"It is a format that you can expect to get very heavy, and isn't meant for "
"sharing on the internet."
msgstr ""
"És un format del que podeu esperar que sigui molt pesat i que no sigui per a "
"compartir per Internet."
