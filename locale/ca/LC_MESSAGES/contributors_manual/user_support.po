# Translation of docs_krita_org_contributors_manual___user_support.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: contributors_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-07-09 03:10+0200\n"
"PO-Revision-Date: 2019-07-10 21:56+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.07.70\n"

#: ../../contributors_manual/user_support.rst:1
msgid "Introduction to user support."
msgstr "Introducció al suport per a l'usuari."

#: ../../contributors_manual/user_support.rst:20
msgid "Introduction to User Support"
msgstr "Introducció al suport per a l'usuari"

#: ../../contributors_manual/user_support.rst:35
msgid "Contents"
msgstr "Contingut"

#: ../../contributors_manual/user_support.rst:38
msgid "Tablet Support"
msgstr "Suport per a la tauleta"

#: ../../contributors_manual/user_support.rst:40
msgid ""
"The majority of help requests are about pen pressure and tablet support in "
"general."
msgstr ""
"La majoria de les sol·licituds d'ajuda són sobre la pressió del llapis i el "
"suport de la tauleta en general."

#: ../../contributors_manual/user_support.rst:44
msgid "Quick solutions"
msgstr "Solucions ràpides"

#: ../../contributors_manual/user_support.rst:46
msgid ""
"On Windows: reinstall your driver (Windows Update often breaks tablet driver "
"settings, reinstallation helps)."
msgstr ""
"Al Windows: torneu a instal·lar el controlador (Windows Update sovint trenca "
"els ajustaments del controlador de la tauleta, la reinstal·lació ajuda)."

#: ../../contributors_manual/user_support.rst:48
msgid ""
"Change API in :menuselection:`Settings --> Configure Krita --> Tablet "
"Settings` (for some devices, especially N-trig ones, Windows Ink work "
"better, for some it's Wintab)."
msgstr ""
"Canvieu l'API a :menuselection:`Arranjament --> Configura el Krita --> "
"Ajustaments per a la tauleta` (per a alguns dispositius -en especial els N-"
"Trig- el Windows Ink funciona millor, per a alguns és el WinTab)."

#: ../../contributors_manual/user_support.rst:50
msgid ""
"On Windows, Wacom tablets: if you get straight lines at the beginnings of "
"the strokes, disable/minimize \"double-click distance\" in Wacom settings."
msgstr ""
"Al Windows, les tauletes Wacom: si obteniu línies rectes al començament dels "
"traços, inhabiliteu/minimitzeu la «distància del doble clic» als ajustaments "
"de Wacom."

#: ../../contributors_manual/user_support.rst:53
msgid "Gathering information"
msgstr "Recopilar la informació"

#: ../../contributors_manual/user_support.rst:55
msgid "Which OS do you use?"
msgstr "Quin sistema operatiu empreu?"

#: ../../contributors_manual/user_support.rst:57
msgid "Which tablet do you have?"
msgstr "Quina tauleta teniu?"

#: ../../contributors_manual/user_support.rst:59
msgid "What is the version of the tablet driver?"
msgstr "Quina és la versió del controlador per a la tauleta?"

# skip-rule: t-acc_obe
#: ../../contributors_manual/user_support.rst:61
msgid ""
"Please collect Tablet Tester (:menuselection:`Settings --> Configure Krita --"
"> Tablet Settings`) output, paste it to `Pastebin <https://pastebin.com/>`_ "
"or similar website and give us a link."
msgstr ""
"Si us plau, recopileu la sortida del Provador de la tauleta (:menuselection:"
"`Arranjament --> Configura el Krita --> Ajustaments per a la tauleta`), "
"enganxeu-la al lloc web `Pastebin <https://pastebin.com/>`_ o similar i "
"envieu-nos un enllaç."

#: ../../contributors_manual/user_support.rst:65
msgid "Additional information for supporters"
msgstr "Informació addicional per als que donen suport"

#: ../../contributors_manual/user_support.rst:67
msgid ""
"Except for the issue with beginnings of the strokes, Wacom tablets usually "
"work no matter the OS."
msgstr ""
"A excepció del problema amb el començament dels traços, les tauletes Wacom "
"generalment funcionen sense importar el sistema operatiu."

#: ../../contributors_manual/user_support.rst:69
msgid ""
"Huion tablets should work on Windows and on Linux, on Mac there might be "
"issues."
msgstr ""
"Les tauletes Huion haurien de funcionar al Windows i Linux, sobre Mac hi "
"podria haver problemes."

#: ../../contributors_manual/user_support.rst:71
msgid "XP-Pen tablets and other brands can have issues everywhere."
msgstr ""
"Les tauletes XP-Pen i altres marques poden tenir problemes a tot arreu."

#: ../../contributors_manual/user_support.rst:73
msgid ""
"If someone asks about a tablet to buy, generally a cheaper Wacom or a Huion "
"are the best options as of 2019, if they want to work with Krita. :ref:"
"`list_supported_tablets`"
msgstr ""
"Si algú pregunta per una tauleta per comprar, generalment, les Wacom o Huion "
"són més barates i també són les millors opcions a partir de 2019 si volem "
"treballar amb el Krita. :ref:`list_supported_tablets`."

# skip-rule: t-acc_obe
#: ../../contributors_manual/user_support.rst:75
msgid ""
"`Possibly useful instruction in case of XP-Pen tablet issues <https://www."
"reddit.com/r/krita/comments/btzh72/"
"xppen_artist_12s_issue_with_krita_how_to_fix_it/>`_."
msgstr ""
"`Instruccions possiblement útils en el cas de problemes amb la tauleta XP-"
"Pen <https://www.reddit.com/r/krita/comments/btzh72/"
"xppen_artist_12s_issue_with_krita_how_to_fix_it/>`_."

#: ../../contributors_manual/user_support.rst:79
msgid "Animation"
msgstr "Animació"

#: ../../contributors_manual/user_support.rst:81
msgid ""
"Issues with rendering animation can be of various shapes and colors. First "
"thing to find out is whether the issue happens on Krita's or FFmpeg's side "
"(Krita saves all the frames, then FFmpeg is used to render a video using "
"this sequence of images). To learn that, instruct the user to render as "
"\"Image Sequence\". If the image sequence is correct, FFmpeg (or more often: "
"render options) are at fault. If the image sequence is incorrect, either the "
"options are wrong (if for example not every frame got rendered), or it's a "
"bug in Krita."
msgstr ""
"Els problemes amb la renderització de l'animació poden ser de diverses "
"formes i colors. El primer que hem d'esbrinar és si el problema succeeix al "
"costat del Krita o de FFmpeg (el Krita desa tots els fotogrames i després "
"utilitza el FFmpeg per a renderitzar un vídeo emprant aquesta seqüència "
"d'imatges). Per esbrinar això, indiqueu a l'usuari que renderitzi com a "
"«Seqüència d'imatges». Si la seqüència d'imatges és correcta, el FFmpeg (o "
"més sovint: les opcions de renderitzat) tenen la culpa. Si la seqüència "
"d'imatges no és correcta, les opcions seran incorrectes (si, per exemple, no "
"s'han renderitzat tots els fotogrames) o serà un error en el Krita."

#: ../../contributors_manual/user_support.rst:85
msgid ""
"If the user opens the Log Viewer docker, turns on logging and then tries to "
"render a video, Krita will print out the whole ffmpeg command to Log Viewer "
"so it can be easily investigated."
msgstr ""
"Si l'usuari obre l'acoblador Visor del registre, activa el registre i "
"després intenta reproduir un vídeo, el Krita imprimirà tota la sortida de "
"l'ordre «ffmpeg» al Visor del registre perquè es pugui investigar amb "
"facilitat."

#: ../../contributors_manual/user_support.rst:87
msgid ""
"There is a log file in the directory that user tries to render to. It can "
"contain information useful to investigation of the issue."
msgstr ""
"Hi haurà un fitxer de registre en el directori on l'usuari intenta "
"renderitzar. Pot contenir informació útil per a la investigació del problema."

#: ../../contributors_manual/user_support.rst:90
msgid "Onion skin issues"
msgstr "Problemes amb la Pell de ceba"

#: ../../contributors_manual/user_support.rst:92
msgid ""
"The great majority of issues with onion skin are just user errors, not bugs. "
"Nonetheless, you need to find out why it happens and direct the user how to "
"use onion skin properly."
msgstr ""
"La gran majoria dels problemes amb la pell de ceba són errors de l'usuari, "
"no errors del programari en sí. No obstant això, haureu d'esbrinar perquè "
"succeeix i dirigir a l'usuari sobre com fer servir la pell de ceba de manera "
"adequada."

#: ../../contributors_manual/user_support.rst:96
msgid "Crash"
msgstr "Fallada"

#: ../../contributors_manual/user_support.rst:98
msgid ""
"In case of crash try to determine if the problem is known, if not, instruct "
"user to create a bug report (or create it yourself) with following "
"information:"
msgstr ""
"En el cas de fallada, intenteu determinar si el problema és conegut, si no, "
"indiqueu a l'usuari que creï un informe d'error (o creu-lo vosaltres "
"mateixos) amb la següent informació:"

#: ../../contributors_manual/user_support.rst:100
msgid "What happened, what was being done just before the crash."
msgstr "Què ha passat? Què s'estava fent just abans de la fallada?"

# skip-rule: t-sc-yes
#: ../../contributors_manual/user_support.rst:102
msgid ""
"Is it possible to reproduce (repeat)? If yes, provide a step-by-step "
"instruction to get the crash."
msgstr ""
"És possible reproduir-la (repetir)? En cas afirmatiu, proporcioneu una "
"instrucció pas a pas sobre com obtenir la fallada."

#: ../../contributors_manual/user_support.rst:104
msgid ""
"Backtrace (crashlog) -- the instruction is here: :ref:`dr_minw`, and the "
"debug symbols can be found in the annoucement of the version of Krita that "
"the user has. But it could be easier to just point the user to `https://"
"download.kde.org/stable/krita <https://download.kde.org/stable/krita>`_."
msgstr ""
"Traça inversa (registre de la fallada): les instruccions estan aquí, el :ref:"
"`dr_minw` i els símbols de depuració es troben a l'anunci de la versió del "
"Krita que té l'usuari. Però podria ser més fàcil simplement assenyalar a "
"l'usuari el lloc `https://download.kde.org/stable/krita <https://download."
"kde.org/stable/krita>`_."

#: ../../contributors_manual/user_support.rst:108
msgid "Other possible questions with quick solutions"
msgstr "Altres preguntes possibles amb solucions ràpides"

#: ../../contributors_manual/user_support.rst:110
msgid ""
"When the user has trouble with anything related to preview or display, ask "
"them to change :guilabel:`Canvas Graphics Acceleration` in :menuselection:"
"`Settings --> Configure Krita --> Display`."
msgstr ""
"Quan l'usuari tingui problemes amb qualsevol cosa relacionada amb la vista "
"prèvia o la pantalla, demaneu-li que canviï l'opció :guilabel:`Acceleració "
"dels gràfics del llenç` a :menuselection:`Arranjament --> Configura el Krita "
"--> Pantalla`."

#: ../../contributors_manual/user_support.rst:116
msgid ""
"When the user has any weird issue, something you've never heard about, ask "
"them to reset the configuration: :ref:`faq_reset_krita_configuration`."
msgstr ""
"Quan l'usuari tingui algun problema estrany, qualsevulla cosa del que mai "
"heu sentit a parlar, demaneu-li que restableixi la configuració: :ref:"
"`faq_reset_krita_configuration`."

#: ../../contributors_manual/user_support.rst:120
msgid "Advices for supporters"
msgstr "Consells per als que donen suport"

#: ../../contributors_manual/user_support.rst:122
msgid ""
"If you don't understand the question, ask for clarification -- asking for a "
"screen recording or a screenshot is perfectly fine."
msgstr ""
"Si no enteneu la pregunta, demaneu un aclariment: sol·licitar un "
"enregistrament de la pantalla o una captura de pantalla és perfectament "
"correcte."

#: ../../contributors_manual/user_support.rst:124
msgid ""
"If you don't know the solution but you know what information will be needed "
"to investigate the issue further, don't hesitate to ask. Other supporters "
"may know the answer, but have too little time to move the user through the "
"whole process, so you're helping a lot just by asking for additional "
"information. This is very much true in case of tablet issues, for example."
msgstr ""
"Si no coneixeu la solució però sabeu quina informació es necessitarà per "
"investigar el problema més a fons, no dubteu a preguntar. Altra gent que "
"dóna suport poden conèixer la resposta, però tindrem molt poc temps per "
"guiar a l'usuari a través de tot el procés, pel que ajudareu molt només "
"demanant informació addicional. Això és molt cert en el cas de problemes amb "
"la tauleta, per exemple."

#: ../../contributors_manual/user_support.rst:126
msgid ""
"If you don't know the answer/solution and the question looks abandoned by "
"other supporters, you can always ask for help on Krita IRC channel. It's "
"#krita on freenote.net: :ref:`the_krita_community`."
msgstr ""
"Si no coneixeu la resposta/solució i la pregunta sembla abandonada per "
"altres que donen suport, sempre podreu demanar ajuda al canal IRC del Krita. "
"Aquest és el #krita al servidor de freenote.net: :ref:`the_krita_community`."

#: ../../contributors_manual/user_support.rst:128
msgid ""
"Explain steps the user needs to make clearly, for example if you need them "
"to change something in settings, clearly state the whole path of buttons and "
"tabs to get there."
msgstr ""
"Expliqueu amb claredat les passes que haurà de fer l'usuari, per exemple, si "
"necessiteu canviar quelcom als ajustaments, establiu amb claredat tot el "
"camí cap els botons i pestanyes per arribar-hi."

#: ../../contributors_manual/user_support.rst:130
msgid ""
"Instead of :menuselection:`Settings --> Configure Krita` use just :"
"menuselection:`Configure Krita` -- it's easy enough to find and Mac users "
"(where you need to select :menuselection:`Krita --> Settings`) won't get "
"confused."
msgstr ""
"En lloc d':menuselection:`Arranjament --> Configura el Krita` simplement "
"empreu :menuselection:`Configura el Krita`: és força fàcil de trobar i els "
"usuaris de Mac (on necessiten seleccionar :menuselection:`Krita --> "
"Arranjament`) no es confondran."

# skip-rule: t-acc_obe
#: ../../contributors_manual/user_support.rst:132
msgid ""
"If you ask for an image, mention usage of `Imgur <https://imgur.com>`_ or "
"`Pasteboard <https://pasteboard.co>`_, otherwise Reddit users might create a "
"new post with this image instead of including it to the old conversation."
msgstr ""
"Si demaneu una imatge, esmenteu l'ús d'`Imgur <https://imgur.com>`_ o "
"`Pasteboard <https://pasteboard.co>`_, en cas contrari, els usuaris de "
"Reddit podrien crear una nova publicació amb aquesta imatge en lloc "
"d'incloure-la en la conversa anterior."

#: ../../contributors_manual/user_support.rst:134
msgid ""
"If you want to quickly answer someone, just link to the appropriate place in "
"this manual page -- you can click on the little link icon next to the "
"section or subsection title and give the link to the user so they for "
"example know what information about their tablet issue you need."
msgstr ""
"Si voleu respondre amb rapidesa a algú, simplement apunteu l'enllaç al lloc "
"adequat en aquesta pàgina de manual. Podeu fer clic a la petita icona "
"d'enllaç que es troba al costat del títol de la secció o subsecció i lliureu-"
"li a l'usuari perquè, per exemple, sàpiga quina informació necessiteu sobre "
"el problema en la seva tauleta."

#: ../../contributors_manual/user_support.rst:136
msgid ""
"If the user access the internet from the country or a workplace with some of "
"the websites blocked (like imgur.com or pastebin.com), here is a list of "
"alternatives that works:"
msgstr ""
"Si l'usuari accedeix a Internet des d'un país o des d'un lloc de treball amb "
"alguns llocs web bloquejats (com imgur.com o pastebin.com), aquí hi ha una "
"llista d'alternatives que funcionen:"

# skip-rule: t-acc_obe
#: ../../contributors_manual/user_support.rst:138
msgid "Images (e.g. screenshots): `Pasteboard <https://pasteboard.co>`_"
msgstr ""
"Imatges (p. ex. captures de pantalla): `Pasteboard <https://pasteboard.co>`_"

# skip-rule: t-acc_obe
#: ../../contributors_manual/user_support.rst:140
msgid ""
"Text only: `BPaste <https://bpaste.net>`_, `paste.ubuntu.org.cn <paste."
"ubuntu.org.cn>`_, `paste.fedoraproject.org <https://paste.fedoraproject.org/"
">`_ or `https://invent.kde.org/dashboard/snippets (needs KDE Identity) "
"<https://invent.kde.org/dashboard/snippets>`_."
msgstr ""
"Només text: `BPaste <https://bpaste.net>`_, `paste.ubuntu.org.cn <https://"
"paste.ubuntu.org.cn/>`_, `paste.fedoraproject.org <https://paste."
"fedoraproject.org/>`_ o `https://invent.kde.org/dashboard/snippets "
"(necessita la KDE Identity) <https://invent.kde.org/dashboard/snippets>`_."

# skip-rule: t-acc_obe
#: ../../contributors_manual/user_support.rst:142
msgid ""
"``.kra`` and other formats: by mail? Or encode the file using `base64` "
"command on Linux, send by mail or on Pastebin, then decode using the same "
"command."
msgstr ""
"El format ``.kra`` i altres: enviar per correu? O que codifiqui el fitxer "
"emprant l'ordre «base64» a Linux, envieu-lo per correu o amb Pastebin, "
"després podreu descodificar-la emprant la mateixa ordre."

#: ../../contributors_manual/user_support.rst:147
msgid ""
"If you ask user to store their log or other data on a website, make sure it "
"stays there long enough for you to get it -- for example bpaste.net stores "
"files by default only for a day! And you can extend it only to one week."
msgstr ""
"Si li demaneu a l'usuari que emmagatzemi el seu registre o altres dades en "
"un lloc web, assegureu-vos que romanguin allà prou temps perquè les "
"obtingueu -per exemple, de manera predeterminada, el servei de bpaste.net "
"emmagatzema els fitxers només per un dia, i només ho podreu estendre a una "
"setmana-."

#: ../../contributors_manual/user_support.rst:149
msgid ""
"Make sure they don't post their personal data. Tablet Tester log is safe, "
"log from the :menuselection:`Help -> Show system information for bug "
"reports` might not be that safe. Maybe you could ask them to send it to you "
"by mail?"
msgstr ""
"Comproveu que no publiquin les seves dades personals. El registre del "
"Provador de la tauleta és segur, el registre des de l'element de menú :"
"menuselection:`Ajuda --> Mostra la informació del sistema pels informes "
"d'error` no es tan segur. Potser podríeu demanar-li que us l'enviï per "
"correu?"
