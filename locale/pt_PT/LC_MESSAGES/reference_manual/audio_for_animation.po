# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-03-17 10:53+0000\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: libasound good libqt Krita gstreamer pulseaudio dev\n"
"X-POFile-SpellExtra: OGM bad multimedia libgstreamer guilabel\n"
"X-POFile-SpellExtra: menuselection\n"

#: ../../reference_manual/audio_for_animation.rst:1
msgid "The audio playback with animation in Krita."
msgstr "A reprodução de áudio com animações no Krita."

#: ../../reference_manual/audio_for_animation.rst:12
msgid "Animation"
msgstr "Animação"

#: ../../reference_manual/audio_for_animation.rst:12
msgid "Audio"
msgstr "Áudio"

#: ../../reference_manual/audio_for_animation.rst:12
msgid "Sound"
msgstr "Som"

#: ../../reference_manual/audio_for_animation.rst:12
msgid "Timeline"
msgstr "Linha Temporal"

#: ../../reference_manual/audio_for_animation.rst:17
msgid "Audio for Animation"
msgstr "Áudio para Animações"

#: ../../reference_manual/audio_for_animation.rst:21
msgid ""
"Audio for animation is an unfinished feature. It has multiple bugs and may "
"not work on your system."
msgstr ""
"O áudio nas animações é uma funcionalidade ainda por concluir. Tem muitos "
"erros e poderá não funcionar no seu sistema."

#: ../../reference_manual/audio_for_animation.rst:23
msgid ""
"You can add audio files to your animation to help sync lips or music. This "
"functionality is available in the timeline docker."
msgstr ""
"Poderá adicionar ficheiros de áudio à sua animação para ajudar na "
"sincronização de lábios ou com a música. Esta funcionalidade está disponível "
"na área da linha temporal."

#: ../../reference_manual/audio_for_animation.rst:26
msgid "Importing Audio Files"
msgstr "Importar Ficheiros de Áudio"

#: ../../reference_manual/audio_for_animation.rst:28
msgid ""
"Krita supports MP3, OGM, and WAV audio files. When you open up your timeline "
"docker, there will be a speaker button in the top left area."
msgstr ""
"O Krita suporta ficheiros de áudio MP3, OGM e WAV. Quando abrir a área da "
"linha temporal, existirá um botão com um altifalante na área superior "
"esquerda."

#: ../../reference_manual/audio_for_animation.rst:30
msgid ""
"If you press the speaker button, you will get the available audio options "
"for the animation."
msgstr ""
"Se carregar nesse botão, irá obter as opções de áudio disponíveis para a "
"animação."

#: ../../reference_manual/audio_for_animation.rst:32
msgid "Open"
msgstr "Abrir"

#: ../../reference_manual/audio_for_animation.rst:33
msgid "Mute"
msgstr "Silenciar"

#: ../../reference_manual/audio_for_animation.rst:34
msgid "Remove audio"
msgstr "Remover o áudio"

#: ../../reference_manual/audio_for_animation.rst:35
msgid "Volume slider"
msgstr "Barra do volume"

#: ../../reference_manual/audio_for_animation.rst:37
msgid ""
"Krita saves the location of your audio file. If you move the audio file or "
"rename it, Krita will not be able to find it. Krita will tell you the file "
"was moved or deleted the next time you try to open the Krita file up."
msgstr ""
"O Krita grava a localização do seu ficheiro de áudio. Se mover o ficheiro ou "
"mudar o nome do mesmo, o Krita não conseguirá encontrá-lo. O Krita irá "
"indicar-lhe que o ficheiro foi movido ou apagado da próxima vez que tentar "
"abrir o ficheiro no Krita."

#: ../../reference_manual/audio_for_animation.rst:40
msgid "Using Audio"
msgstr "Usar o Áudio"

#: ../../reference_manual/audio_for_animation.rst:42
msgid ""
"After you import the audio, you can scrub through the timeline and it will "
"play the audio chunk at the time spot. When you press the Play button, the "
"entire the audio file will playback as it will in the final version. There "
"is no visual display of the audio file on the screen, so you will need to "
"use your ears and the scrubbing functionality to position frames."
msgstr ""
"Depois de importar o áudio, poderá percorrer a linha temporal, para que ela "
"reproduza o excerto de áudio no ponto exacto no tempo. Quando carregar no "
"botão Reproduzir, o ficheiro de áudio inteiro será reproduzido como ficará "
"na versão final. Não existe nenhuma visualização do ficheiro de áudio no "
"ecrã, pelo que terá de usar os seus ouvidos e a funcionalidade de circulação "
"para posicionar as imagens."

#: ../../reference_manual/audio_for_animation.rst:46
msgid "Exporting with Audio"
msgstr "Exportação com Áudio"

#: ../../reference_manual/audio_for_animation.rst:48
msgid ""
"To get the audio file included when you are exporting, you need to include "
"it in the Render Animation options. In the :menuselection:`File --> Render "
"Animation` options there is a checkbox :guilabel:`Include Audio`. Make sure "
"that is checked before you export and you should be good to go."
msgstr ""
"Para incluir o ficheiro de áudio quando estiver a exportar, terá de o "
"incluir nas opções para Desenhar a Animação. Nas opções em :menuselection:"
"`Ficheiro --> Desenhar a Animação`, existe uma opção de marcação :guilabel:"
"`Incluir o Áudio`. Certifique-se que está assinalada antes da exportação, e "
"tudo deverá estar pronto a usar."

#: ../../reference_manual/audio_for_animation.rst:51
msgid "Packages needed for Audio on Linux"
msgstr "Pacotes necessários para o Áudio no Linux"

#: ../../reference_manual/audio_for_animation.rst:53
msgid ""
"The following packages are necessary for having the audio support on Linux:"
msgstr ""
"São necessários os seguintes pacotes para ter o suporte de áudio no Linux:"

#: ../../reference_manual/audio_for_animation.rst:56
msgid "For people who build Krita on Linux:"
msgstr "Para as pessoas que tenham compilado o Krita no Linux:"

#: ../../reference_manual/audio_for_animation.rst:58
msgid "libasound2-dev"
msgstr "libasound2-dev"

#: ../../reference_manual/audio_for_animation.rst:59
msgid "libgstreamer1.0-dev gstreamer1.0-pulseaudio"
msgstr "libgstreamer1.0-dev gstreamer1.0-pulseaudio"

#: ../../reference_manual/audio_for_animation.rst:60
msgid "libgstreamer-plugins-base1.0-dev"
msgstr "libgstreamer-plugins-base1.0-dev"

#: ../../reference_manual/audio_for_animation.rst:61
msgid "libgstreamer-plugins-good1.0-dev"
msgstr "libgstreamer-plugins-good1.0-dev"

#: ../../reference_manual/audio_for_animation.rst:62
msgid "libgstreamer-plugins-bad1.0-dev"
msgstr "libgstreamer-plugins-bad1.0-dev"

#: ../../reference_manual/audio_for_animation.rst:64
msgid "For people who use Krita on Linux:"
msgstr "Para as pessoas que usem o Krita no Linux:"

#: ../../reference_manual/audio_for_animation.rst:66
msgid "libqt5multimedia5-plugins"
msgstr "libqt5multimedia5-plugins"

#: ../../reference_manual/audio_for_animation.rst:67
msgid "libgstreamer-plugins-base1.0"
msgstr "libgstreamer-plugins-base1.0"

#: ../../reference_manual/audio_for_animation.rst:68
msgid "libgstreamer-plugins-good1.0"
msgstr "libgstreamer-plugins-good1.0"

#: ../../reference_manual/audio_for_animation.rst:69
msgid "libgstreamer-plugins-bad1.0"
msgstr "libgstreamer-plugins-bad1.0"
