# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-08 17:30+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: symbols Krita menuselection Inkscape\n"

#: ../../reference_manual/dockers/vector_library.rst:1
msgid "Overview of the vector library docker."
msgstr "Introdução à área da biblioteca vectorial."

#: ../../reference_manual/dockers/vector_library.rst:10
#: ../../reference_manual/dockers/vector_library.rst:15
msgid "Vector Library"
msgstr "Biblioteca Vectorial"

#: ../../reference_manual/dockers/vector_library.rst:10
msgid "SVG Symbols"
msgstr "Símbolos em SVG"

#: ../../reference_manual/dockers/vector_library.rst:10
msgid "Reusable Vector Shapes"
msgstr "Formas Vectoriais Reutilizáveis"

#: ../../reference_manual/dockers/vector_library.rst:19
msgid ""
"The Vector Library Docker loads the symbol libraries in SVG files, when "
"those SVG files are put into the \"symbols\" folder in the resource folder :"
"menuselection:`Settings --> Manage Resources --> Open Resource Folder`."
msgstr ""
"A área da Biblioteca Vectorial carrega as bibliotecas de símbolos em "
"ficheiros SVG, onde esses ficheiro SVG são colocados na pasta \"symbols\" da "
"pasta de recursos definida em :menuselection:`Configuração --> Gerir os "
"Recursos --> Abrir a Pasta de Recursos`."

#: ../../reference_manual/dockers/vector_library.rst:21
msgid ""
"The vector symbols can then be dragged and dropped onto the canvas, allowing "
"you to quickly use complicated images."
msgstr ""
"Poderá então arrastar e largar os símbolos vectoriais na área de desenho, "
"permitindo-lhe usar rapidamente imagens complicadas."

#: ../../reference_manual/dockers/vector_library.rst:23
msgid ""
"Currently, you cannot make symbol libraries with Krita yet, but you can make "
"them by hand, as well as use Inkscape to make them. Thankfully, there's "
"quite a few svg symbol libraries out there already!"
msgstr ""
"De momento, ainda não poderá criar bibliotecas de símbolos com o Krita, mas "
"podê-las-á criar manualmente, assim como usar o Inkscape para as criar. "
"Felizmente, já existe algumas bibliotecas de símbolos em SVG por aí!"
