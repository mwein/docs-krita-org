# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-11 03:18+0200\n"
"PO-Revision-Date: 2019-06-11 18:25+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../<generated>:1
msgid "Guides"
msgstr "Hjälplinjer"

#: ../../reference_manual/dockers/snap_settings_docker.rst:1
msgid "Overview of the snap settings docker."
msgstr "Översikt av låsinställningspanelen."

#: ../../reference_manual/dockers/snap_settings_docker.rst:11
msgid "Snap"
msgstr "Lås"

#: ../../reference_manual/dockers/snap_settings_docker.rst:16
msgid "Snap Settings"
msgstr "Låsinställningar"

#: ../../reference_manual/dockers/snap_settings_docker.rst:20
msgid ""
"This docker has been removed in Krita 3.0. For more information on how to do "
"this instead, consult the :ref:`snapping page <snapping>`."
msgstr ""
"Panelen har tagits bort i Krita 3.0. För mer information om hur man gör det "
"istället, konsultera :ref:`låsningssidan <snapping>`."

#: ../../reference_manual/dockers/snap_settings_docker.rst:23
msgid ".. image:: images/dockers/Krita_Snap_Settings_Docker.png"
msgstr ".. image:: images/dockers/Krita_Snap_Settings_Docker.png"

#: ../../reference_manual/dockers/snap_settings_docker.rst:24
msgid ""
"This is docker only applies for Vector Layers. Snapping determines where a "
"vector shape will snap. The little number box is for snapping to a grid."
msgstr ""
"Den här panelen gäller bara vektorlager. Låsning bestämmer var en vektorform "
"kommer att låsas. Den lilla nummerrutan är för att låsa till ett rutnät."

#: ../../reference_manual/dockers/snap_settings_docker.rst:26
msgid "Node"
msgstr "Nod"

#: ../../reference_manual/dockers/snap_settings_docker.rst:27
msgid "For snapping to other vector nodes."
msgstr "För att låsa till andra vektornoder."

#: ../../reference_manual/dockers/snap_settings_docker.rst:28
msgid "Extensions of Line"
msgstr "Förlängning av linje"

#: ../../reference_manual/dockers/snap_settings_docker.rst:29
msgid ""
"For snapping to a point that could have been part of a line, had it been "
"extended."
msgstr ""
"För att låsa till en punkt som kunde ha ingått i en linje om den hade "
"förlängts"

#: ../../reference_manual/dockers/snap_settings_docker.rst:30
msgid "Bounding Box"
msgstr "Omgivande ruta"

#: ../../reference_manual/dockers/snap_settings_docker.rst:31
msgid "For snapping to the bounding box of a vector shape."
msgstr "För att låsa till en vektorforms omgivande ruta."

#: ../../reference_manual/dockers/snap_settings_docker.rst:32
msgid "Orthogonal"
msgstr "Vinkelrät"

#: ../../reference_manual/dockers/snap_settings_docker.rst:33
msgid "For snapping to only horizontal or vertical lines."
msgstr "För att bara låsa till horisontella och vertikala linjer."

#: ../../reference_manual/dockers/snap_settings_docker.rst:34
msgid "Intersection"
msgstr "Linjekorsning"

#: ../../reference_manual/dockers/snap_settings_docker.rst:35
msgid "For snapping to other vector lines."
msgstr "För att låsa till andra vektorlinjer."

#: ../../reference_manual/dockers/snap_settings_docker.rst:37
msgid "Guides don't exist in Krita, therefore this one is useless."
msgstr "Hjälplinjer finns inte i Krita, och därför är den här oanvändbar."
