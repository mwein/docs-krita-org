# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-04 11:17+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../reference_manual/dockers/task_sets.rst:1
msgid "Overview of the task sets docker."
msgstr "Översikt av panelen Aktivitetsuppsättningar."

#: ../../reference_manual/dockers/task_sets.rst:16
msgid "Task Sets Docker"
msgstr "Panelen Aktivitetsuppsättningar"

#: ../../reference_manual/dockers/task_sets.rst:18
msgid ""
"Task sets are for sharing a set of steps, like a tutorial. You make them "
"with the task-set docker."
msgstr ""
"Aktivitetsuppsättningar är till för att dela ett antal steg, som en "
"handledning. De skapas med panelen aktivitetsuppsättningar."

#: ../../reference_manual/dockers/task_sets.rst:21
msgid ".. image:: images/dockers/Task-set.png"
msgstr ".. image:: images/dockers/Task-set.png"

#: ../../reference_manual/dockers/task_sets.rst:22
msgid ""
"Task sets can record any kind of command also available via the shortcut "
"manager. It can not record strokes, like the macro recorder can. However, "
"you can play macros with the tasksets!"
msgstr ""
"Aktivitetsuppsättningar kan spela in alla sorters kommandon, också "
"tillgängliga via genvägshanteraren. De kan inte spela in gester såsom "
"makroinspelningen kan. Dock kan man spela upp makron med "
"aktivitetsuppsättningarna."

#: ../../reference_manual/dockers/task_sets.rst:24
msgid ""
"The tasksets docker has a record button, and you can use this to record a "
"certain workflow. Then use this to let items appear in the taskset list. "
"Afterwards, turn off the record. You can then click any action in the list "
"to make them happen. Press the 'Save' icon to name and save the taskset."
msgstr ""
"Panelen Aktivitetsuppsättningar har en inspelningsknapp, som kan användas "
"för att spela in ett visst arbetsflöde. Använd den för att låta objekt dyka "
"upp i listan över aktivitetsuppsättningar. Stäng av inspelning efteråt. "
"Därefter går det att klicka på vilken åtgärd som helst i listan för att "
"utföra den. Klicka på ikonen 'Spara' för att namnge och spara "
"aktivitetsuppsättningen."
