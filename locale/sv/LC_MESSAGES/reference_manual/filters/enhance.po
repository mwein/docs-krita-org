# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-04 11:25+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../reference_manual/filters/enhance.rst:1
msgid "Overview of the enhance filters."
msgstr "Översikt av förbättringsfiltren."

#: ../../reference_manual/filters/enhance.rst:10
#: ../../reference_manual/filters/enhance.rst:19
msgid "Sharpen"
msgstr "Gör skarpare"

#: ../../reference_manual/filters/enhance.rst:10
msgid "Filters"
msgstr "Filter"

#: ../../reference_manual/filters/enhance.rst:15
msgid "Enhance"
msgstr "Förbättra"

#: ../../reference_manual/filters/enhance.rst:17
msgid ""
"These filters all focus on reducing the blur in the image by sharpening and "
"enhancing details and the edges. Following are various sharpen and enhance "
"filters in provided in Krita."
msgstr ""
"Dessa filter fokuserar alla på att reducera suddighet på en bild genom att "
"skärpa och förbättra detaljer och kanter. Det följande är diverse "
"skärpefilter och förbättringsfilter som tillhandahålls i Krita."

#: ../../reference_manual/filters/enhance.rst:20
msgid "Mean Removal"
msgstr "Medelvärdesborttagning"

#: ../../reference_manual/filters/enhance.rst:21
msgid "Unsharp Mask"
msgstr "Oskarp mask"

#: ../../reference_manual/filters/enhance.rst:22
msgid "Gaussian Noise reduction"
msgstr "Reducering av Gaussiskt brus"

#: ../../reference_manual/filters/enhance.rst:23
msgid "Wavelet Noise Reducer"
msgstr "Brusreducering med vågelement"
