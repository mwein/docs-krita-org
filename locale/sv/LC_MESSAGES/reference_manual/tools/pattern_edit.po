# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-15 03:16+0200\n"
"PO-Revision-Date: 2019-06-17 18:00+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../<rst_epilog>:20
msgid ""
".. image:: images/icons/pattern_tool.svg\n"
"   :alt: toolpatternedit"
msgstr ""
".. image:: images/icons/pattern_tool.svg\n"
"   :alt: Mönsterredigeringsverktyg"

#: ../../reference_manual/tools/pattern_edit.rst:1
msgid "Krita's vector pattern editing tool reference."
msgstr "Referens för Kritas mönsterredigeringsverktyg."

#: ../../reference_manual/tools/pattern_edit.rst:11
msgid "Tools"
msgstr "Verktyg"

#: ../../reference_manual/tools/pattern_edit.rst:11
msgid "Pattern"
msgstr "Mönster"

#: ../../reference_manual/tools/pattern_edit.rst:16
msgid "Pattern Editing Tool"
msgstr "Mönsterredigeringsverktyg"

#: ../../reference_manual/tools/pattern_edit.rst:18
msgid "|toolpatternedit|"
msgstr "|toolpatternedit|"

#: ../../reference_manual/tools/pattern_edit.rst:22
msgid ""
"The pattern editing tool has been removed in 4.0, currently there's no way "
"to edit pattern fills for vectors."
msgstr ""
"Mönsterredigeringsverktyget har tagits bort i 4.0, och för närvarande finns "
"det inget sätt att redigera ifyllnad av mönster för vektorer."

#: ../../reference_manual/tools/pattern_edit.rst:24
msgid ""
"The Pattern editing tool works on Vector Shapes that use a Pattern fill. On "
"these shapes, the Pattern Editing Tool allows you to change the size, ratio "
"and origin of a pattern."
msgstr ""
"Mönsterredigeringsverktyget arbetar med vektorformer som använder "
"mönsterfyllnad. För dessa former låter mönsterredigeringsverktyget dig ändra "
"storleken, förhållandet och ursprung."

#: ../../reference_manual/tools/pattern_edit.rst:27
msgid "On Canvas-editing"
msgstr "Redigering på duken"

#: ../../reference_manual/tools/pattern_edit.rst:29
msgid ""
"You can change the origin by click dragging the upper node, this is only "
"possible in Tiled mode."
msgstr ""
"Ursprunget kan ändras genom att klicka och dra den övre noden. Det är bara "
"möjligt för sida vid sida."

#: ../../reference_manual/tools/pattern_edit.rst:31
msgid ""
"You can change the size and ratio by click-dragging the lower node. There's "
"no way to constrain the ratio in on-canvas editing, this is only possible in "
"Original and Tiled mode."
msgstr ""
"Storlek och förhållande kan ändras genom att klicka och dra den nedre noden. "
"Det finns inget sätt att begränsa förhållandet vid redigering på duken, det "
"är bara möjligt med original och sida vid sida."

#: ../../reference_manual/tools/pattern_edit.rst:34
msgid "Tool Options"
msgstr "Verktygsalternativ"

#: ../../reference_manual/tools/pattern_edit.rst:36
msgid "There are several tool options with this tool, for fine-tuning:"
msgstr "Det finns flera verktygsalternativ för verktyget, för finjustering:"

#: ../../reference_manual/tools/pattern_edit.rst:38
msgid "First there are the Pattern options."
msgstr "Först är det mönsteralternativen."

#: ../../reference_manual/tools/pattern_edit.rst:41
msgid "This can be set to:"
msgstr "Den kan ställas in till:"

#: ../../reference_manual/tools/pattern_edit.rst:43
msgid "Original:"
msgstr "Original:"

#: ../../reference_manual/tools/pattern_edit.rst:44
msgid "This will only show one, unstretched, copy of the pattern."
msgstr "Visar bara en, inte utsträckt, kopia av mönstret."

#: ../../reference_manual/tools/pattern_edit.rst:45
msgid "Tiled (Default):"
msgstr "Sida vid sida (förval):"

#: ../../reference_manual/tools/pattern_edit.rst:46
msgid "This will let the pattern appear tiled in the x and y direction."
msgstr "Låter mönstret visas sida vid sida i x- och y-riktningen."

#: ../../reference_manual/tools/pattern_edit.rst:48
msgid "Repeat:"
msgstr "Upprepa:"

#: ../../reference_manual/tools/pattern_edit.rst:48
msgid "Stretch:"
msgstr "Sträck:"

#: ../../reference_manual/tools/pattern_edit.rst:48
msgid "This will stretch the Pattern image to the shape."
msgstr "Sträcker ut mönsterbilden till formen."

#: ../../reference_manual/tools/pattern_edit.rst:51
msgid "Pattern origin. This can be set to:"
msgstr "Mönstrets ursprung. Kan ställas in till:"

#: ../../reference_manual/tools/pattern_edit.rst:53
msgid "Top-left"
msgstr "Uppe till vänster"

#: ../../reference_manual/tools/pattern_edit.rst:54
msgid "Top"
msgstr "Längst upp"

#: ../../reference_manual/tools/pattern_edit.rst:55
msgid "Top-right"
msgstr "Uppe till höger"

#: ../../reference_manual/tools/pattern_edit.rst:56
msgid "Left"
msgstr "Vänster"

#: ../../reference_manual/tools/pattern_edit.rst:57
msgid "Center"
msgstr "Centrera"

#: ../../reference_manual/tools/pattern_edit.rst:58
msgid "Right"
msgstr "Höger"

#: ../../reference_manual/tools/pattern_edit.rst:59
msgid "Bottom-left"
msgstr "Nere till vänster"

#: ../../reference_manual/tools/pattern_edit.rst:60
msgid "Bottom"
msgstr "Längst ner"

#: ../../reference_manual/tools/pattern_edit.rst:61
msgid "Reference point:"
msgstr "Referenspunkt:"

#: ../../reference_manual/tools/pattern_edit.rst:61
msgid "Bottom-right."
msgstr "Nere till höger."

#: ../../reference_manual/tools/pattern_edit.rst:64
msgid "For extra tweaking, set in percentages."
msgstr "För extra finjustering, ställ in det med procentvärden."

#: ../../reference_manual/tools/pattern_edit.rst:66
msgid "X:"
msgstr "X:"

#: ../../reference_manual/tools/pattern_edit.rst:67
msgid "Offset in the X coordinate, so horizontally."
msgstr "Position i x-koordinaten, alltså horisontellt."

#: ../../reference_manual/tools/pattern_edit.rst:69
msgid "Reference Point Offset:"
msgstr "Referenspunktposition:"

#: ../../reference_manual/tools/pattern_edit.rst:69
msgid "Y:"
msgstr "Y:"

#: ../../reference_manual/tools/pattern_edit.rst:69
msgid "Offset in the Y coordinate, so vertically."
msgstr "Position i y-koordinaten, alltså horisontellt."

#: ../../reference_manual/tools/pattern_edit.rst:71
msgid "Tile Offset:"
msgstr "Rutposition:"

#: ../../reference_manual/tools/pattern_edit.rst:72
msgid "The tile offset if the pattern is tiled."
msgstr "Rutposition om mönstret visas sida vid sida."

#: ../../reference_manual/tools/pattern_edit.rst:74
msgid "Fine Tune the resizing of the pattern."
msgstr "Finjustera mönstrets storleksändring."

#: ../../reference_manual/tools/pattern_edit.rst:76
msgid "W:"
msgstr "B:"

#: ../../reference_manual/tools/pattern_edit.rst:77
msgid "The width, in pixels."
msgstr "Bredden i bildpunkter."

#: ../../reference_manual/tools/pattern_edit.rst:79
msgid "Pattern Size:"
msgstr "Mönsterstorlek:"

#: ../../reference_manual/tools/pattern_edit.rst:79
msgid "H:"
msgstr "H:"

#: ../../reference_manual/tools/pattern_edit.rst:79
msgid "The height, in pixels."
msgstr "Höjden i bildpunkter."

#: ../../reference_manual/tools/pattern_edit.rst:81
msgid ""
"And then there's :guilabel:`Patterns`, which is a mini pattern docker, and "
"where you can pick the pattern used for the fill."
msgstr ""
"Sedan finns :guilabel:`Mönster`, som är en mini-mönsterpanel, där man kan "
"välja mönstret som används för ifyllnad."
