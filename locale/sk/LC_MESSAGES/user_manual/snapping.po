# translation of docs_krita_org_user_manual___snapping.po to Slovak
# Roman Paholik <wizzardsk@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_user_manual___snapping\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-03-22 08:24+0100\n"
"Last-Translator: Roman Paholik <wizzardsk@gmail.com>\n"
"Language-Team: Slovak <kde-sk@linux.sk>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 2.0\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: ../../user_manual/snapping.rst:1
msgid "How to use the snapping functionality in Krita."
msgstr ""

#: ../../user_manual/snapping.rst:10 ../../user_manual/snapping.rst:43
msgid "Guides"
msgstr "Vodiace čiary"

#: ../../user_manual/snapping.rst:10
msgid "Snap"
msgstr ""

#: ../../user_manual/snapping.rst:10
msgid "Vector"
msgstr ""

#: ../../user_manual/snapping.rst:15
msgid "Snapping"
msgstr ""

#: ../../user_manual/snapping.rst:17
msgid ""
"In Krita 3.0, we now have functionality for Grids and Guides, but of course, "
"this functionality is by itself not that interesting without snapping."
msgstr ""

#: ../../user_manual/snapping.rst:21
msgid ""
"Snapping is the ability to have Krita automatically align a selection or "
"shape to the grids and guides, document center and document edges. For "
"Vector layers, this goes even a step further, and we can let you snap to "
"bounding boxes, intersections, extrapolated lines and more."
msgstr ""

#: ../../user_manual/snapping.rst:26
msgid ""
"All of these can be toggled using the snap pop-up menu which is assigned to :"
"kbd:`Shift + S` shortcut."
msgstr ""

#: ../../user_manual/snapping.rst:29
msgid "Now, let us go over what each option means:"
msgstr ""

#: ../../user_manual/snapping.rst:32
msgid ""
"This will snap the cursor to the current grid, as configured in the grid "
"docker. This doesn’t need the grid to be visible. Grids are saved per "
"document, making this useful for aligning your art work to grids, as is the "
"case for game sprites and grid-based designs."
msgstr ""

#: ../../user_manual/snapping.rst:34
msgid "Grids"
msgstr "Mriežky"

#: ../../user_manual/snapping.rst:37
msgid "Pixel"
msgstr ""

#: ../../user_manual/snapping.rst:37
msgid ""
"This allows to snap to every pixel under the cursor. Similar to Grid "
"Snapping but with a grid having spacing = 1px and offset = 0px."
msgstr ""

#: ../../user_manual/snapping.rst:40
msgid ""
"This allows you to snap to guides, which can be dragged out from the ruler. "
"Guides do not need to be visible for this, and are saved per document. This "
"is useful for comic panels and similar print-layouts, though we recommend "
"Scribus for more intensive work."
msgstr ""

#: ../../user_manual/snapping.rst:46
#, fuzzy
#| msgid ".. image:: images/en/Snap-orthogonal.png"
msgid ".. image:: images/snapping/Snap-orthogonal.png"
msgstr ".. image:: images/en/Snap-orthogonal.png"

#: ../../user_manual/snapping.rst:48
msgid ""
"This allows you to snap to a horizontal or vertical line from existing "
"vector objects’s nodes (Unless dealing with resizing the height or width "
"only, in which case you can drag the cursor over the path). This is useful "
"for aligning object horizontally or vertically, like with comic panels."
msgstr ""

#: ../../user_manual/snapping.rst:52
msgid "Orthogonal (Vector Only)"
msgstr ""

#: ../../user_manual/snapping.rst:55
#, fuzzy
#| msgid ".. image:: images/en/Snap-node.png"
msgid ".. image:: images/snapping/Snap-node.png"
msgstr ".. image:: images/en/Snap-node.png"

#: ../../user_manual/snapping.rst:57
msgid "Node (Vector Only)"
msgstr ""

#: ../../user_manual/snapping.rst:57
msgid "This snaps a vector node or an object to the nodes of another path."
msgstr ""

#: ../../user_manual/snapping.rst:60
#, fuzzy
#| msgid ".. image:: images/en/Snap-extension.png"
msgid ".. image:: images/snapping/Snap-extension.png"
msgstr ".. image:: images/en/Snap-extension.png"

#: ../../user_manual/snapping.rst:62
msgid ""
"When we draw an open path, the last nodes on either side can be "
"mathematically extended. This option allows you to snap to that. The "
"direction of the node depends on its side handles in path editing mode."
msgstr ""

#: ../../user_manual/snapping.rst:65
msgid "Extension (Vector Only)"
msgstr ""

#: ../../user_manual/snapping.rst:68
#, fuzzy
#| msgid ".. image:: images/en/Snap-intersection.png"
msgid ".. image:: images/snapping/Snap-intersection.png"
msgstr ".. image:: images/en/Snap-intersection.png"

#: ../../user_manual/snapping.rst:69
msgid "Intersection (Vector Only)"
msgstr ""

#: ../../user_manual/snapping.rst:70
msgid "This allows you to snap to an intersection of two vectors."
msgstr ""

#: ../../user_manual/snapping.rst:71
msgid "Bounding box (Vector Only)"
msgstr ""

#: ../../user_manual/snapping.rst:72
msgid "This allows you to snap to the bounding box of a vector shape."
msgstr ""

#: ../../user_manual/snapping.rst:74
msgid "Image bounds"
msgstr "Okraje obrázku"

#: ../../user_manual/snapping.rst:74
msgid "Allows you to snap to the vertical and horizontal borders of an image."
msgstr ""

#: ../../user_manual/snapping.rst:77
msgid "Allows you to snap to the horizontal and vertical center of an image."
msgstr ""

#: ../../user_manual/snapping.rst:78
msgid "Image center"
msgstr "Stred obrázku"

#: ../../user_manual/snapping.rst:80
msgid "The snap works for the following tools:"
msgstr ""

#: ../../user_manual/snapping.rst:82
msgid "Straight line"
msgstr "Priamka"

#: ../../user_manual/snapping.rst:83
msgid "Rectangle"
msgstr "Obdĺžnik"

#: ../../user_manual/snapping.rst:84
msgid "Ellipse"
msgstr "Elipsa"

#: ../../user_manual/snapping.rst:85
msgid "Polyline"
msgstr "Lomená čiara"

#: ../../user_manual/snapping.rst:86
msgid "Path"
msgstr "Cesta"

#: ../../user_manual/snapping.rst:87
msgid "Freehand path"
msgstr "Cesta voľnou rukou"

#: ../../user_manual/snapping.rst:88
msgid "Polygon"
msgstr "Mnohouholník"

#: ../../user_manual/snapping.rst:89
msgid "Gradient"
msgstr "Prechod"

#: ../../user_manual/snapping.rst:90
msgid "Shape Handling tool"
msgstr ""

#: ../../user_manual/snapping.rst:91
msgid "The Text-tool"
msgstr ""

#: ../../user_manual/snapping.rst:92
msgid "Assistant editing tools"
msgstr ""

#: ../../user_manual/snapping.rst:93
msgid ""
"The move tool (note that it snaps to the cursor position and not the "
"bounding box of the layer, selection or whatever you are trying to move)"
msgstr ""

#: ../../user_manual/snapping.rst:96
msgid "The Transform tool"
msgstr ""

#: ../../user_manual/snapping.rst:97
msgid "Rectangle select"
msgstr ""

#: ../../user_manual/snapping.rst:98
msgid "Elliptical select"
msgstr ""

#: ../../user_manual/snapping.rst:99
msgid "Polygonal select"
msgstr ""

#: ../../user_manual/snapping.rst:100
msgid "Path select"
msgstr ""

#: ../../user_manual/snapping.rst:101
msgid "Guides themselves can be snapped to grids and vectors"
msgstr ""

#: ../../user_manual/snapping.rst:103
msgid ""
"Snapping doesn’t have a sensitivity yet, and by default is set to 10 screen "
"pixels."
msgstr ""
