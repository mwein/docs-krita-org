msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-02-27 01:35+0100\n"
"Last-Translator: KDE Francophone <kde-francophone@kde.org>\n"
"Language-Team: KDE Francophone <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 1.5\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Environment: kde\n"
"X-Language: fr_FR\n"
"X-Qt-Contexts: true\n"
"Generated-By: Babel 0.9.6\n"
"X-Source-Language: C\n"

#: ../../reference_manual/dockers/digital_color_mixer.rst:1
msgid "Overview of the digital color mixer docker."
msgstr ""

#: ../../reference_manual/dockers/digital_color_mixer.rst:11
#: ../../reference_manual/dockers/digital_color_mixer.rst:16
msgid "Digital Color Mixer"
msgstr ""

#: ../../reference_manual/dockers/digital_color_mixer.rst:11
msgid "Color"
msgstr ""

#: ../../reference_manual/dockers/digital_color_mixer.rst:11
msgid "Color Mixing"
msgstr ""

#: ../../reference_manual/dockers/digital_color_mixer.rst:11
msgid "Color Selector"
msgstr ""

#: ../../reference_manual/dockers/digital_color_mixer.rst:19
msgid ".. image:: images/dockers/Krita_Digital_Color_Mixer_Docker.png"
msgstr ""

#: ../../reference_manual/dockers/digital_color_mixer.rst:20
msgid "This docker allows you to do simple mathematical color mixing."
msgstr ""

#: ../../reference_manual/dockers/digital_color_mixer.rst:22
msgid "It works as follows:"
msgstr ""

#: ../../reference_manual/dockers/digital_color_mixer.rst:24
msgid "You have on the left side the current color."
msgstr ""

#: ../../reference_manual/dockers/digital_color_mixer.rst:26
msgid ""
"Next to that there are six columns. Each of these columns consists of three "
"rows: The lowest row is the color that you are mixing the current color "
"with. Ticking this button allows you to set a different color using a "
"palette and the mini-color wheel. The slider above this mixing color "
"represent the proportions of the mixing color and the current color. The "
"higher the slider, the less of the mixing color will be used in mixing. "
"Finally, the result color. Clicking this will change your current color to "
"the result color."
msgstr ""
