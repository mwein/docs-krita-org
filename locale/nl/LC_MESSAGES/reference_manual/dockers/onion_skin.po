# Dutch translations for Krita Manual package
# Nederlandse vertalingen voor het pakket Krita Manual.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Automatically generated, 2019.
# Freek de Kruijf <freekdekruijf@kde.nl>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-11 03:18+0200\n"
"PO-Revision-Date: 2019-07-08 14:28+0200\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: Dutch <kde-i18n-nl@kde.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.2\n"

#: ../../reference_manual/dockers/onion_skin.rst:1
msgid "Overview of the onion skin docker."
msgstr "Overzicht van de vastzetter Uienskin."

#: ../../reference_manual/dockers/onion_skin.rst:10
msgid "Animation"
msgstr "Animatie"

#: ../../reference_manual/dockers/onion_skin.rst:10
msgid "Onion Skin"
msgstr "Uienskin"

#: ../../reference_manual/dockers/onion_skin.rst:15
msgid "Onion Skin Docker"
msgstr "Vastzetter Uienskin"

#: ../../reference_manual/dockers/onion_skin.rst:18
msgid ".. image:: images/dockers/Onion_skin_docker.png"
msgstr ".. image:: images/dockers/Onion_skin_docker.png"

#: ../../reference_manual/dockers/onion_skin.rst:19
msgid ""
"To make animation easier, it helps to see both the next frame as well as the "
"previous frame sort of layered on top of the current. This is called *onion-"
"skinning*."
msgstr ""

#: ../../reference_manual/dockers/onion_skin.rst:22
msgid ".. image:: images/dockers/Onion_skin_01.png"
msgstr ".. image:: images/dockers/Onion_skin_01.png"

#: ../../reference_manual/dockers/onion_skin.rst:23
msgid ""
"Basically, they are images that represent the frames before and after the "
"current frame, usually colored or tinted."
msgstr ""
"In de basis zijn ze afbeeldingen die de frames representeren voor en na het "
"huidige frame, gewoonlijk gekleurd of getint."

#: ../../reference_manual/dockers/onion_skin.rst:25
msgid ""
"You can toggle them by clicking the lightbulb icon on a layer that is "
"animated (so, has frames), and isn’t fully opaque. (Krita will consider "
"white to be white, not transparent, so don’t animated on an opaque layer if "
"you want onion skins.)"
msgstr ""

#: ../../reference_manual/dockers/onion_skin.rst:29
msgid ""
"Since 4.2 onion skins are disabled on layers whose default pixel is fully "
"opaque. These layers can currently only be created by using :guilabel:"
"`background as raster layer` in the :guilabel:`content` section of the new "
"image dialog. Just don't try to animate on a layer like this if you rely on "
"onion skins, instead make a new one."
msgstr ""

#: ../../reference_manual/dockers/onion_skin.rst:31
msgid ""
"The term onionskin comes from the fact that onions are semi-transparent. In "
"traditional animation animators would make their initial animations on "
"semitransparent paper on top of an light-table (of the special animators "
"variety), and they’d start with so called keyframes, and then draw frames in "
"between. For that, they would place said keyframes below the frame they were "
"working on, and the light table would make the lines of the keyframes shine "
"through, so they could reference them."
msgstr ""

#: ../../reference_manual/dockers/onion_skin.rst:33
msgid ""
"Onion-skinning is a digital implementation of such a workflow, and it’s very "
"useful when trying to animate."
msgstr ""
"Onion-skinning is een digitale implementatie van zo'n workflow en het is erg "
"nuttig bij het proberen van animatie."

#: ../../reference_manual/dockers/onion_skin.rst:36
msgid ".. image:: images/dockers/Onion_skin_02.png"
msgstr ".. image:: images/dockers/Onion_skin_02.png"

#: ../../reference_manual/dockers/onion_skin.rst:37
msgid ""
"The slider and the button with zero offset control the master opacity and "
"visibility of all the onion skins. The boxes at the top allow you to toggle "
"them on and off quickly, the main slider in the middle is a sort of ‘master "
"transparency’ while the sliders to the side allow you to control the "
"transparency per keyframe offset."
msgstr ""

#: ../../reference_manual/dockers/onion_skin.rst:39
msgid ""
"Tint controls how strongly the frames are tinted, the first screen has 100%, "
"which creates a silhouette, while below you can still see a bit of the "
"original colors at 50%."
msgstr ""
"Tint bestuurt hoe sterk de frames een tint krijgen, het eerste scherm heeft "
"100%, wat een silhouet, terwijl met minder u nog steeds een beetje van de "
"originele kleur kunt zien bij 50%."

#: ../../reference_manual/dockers/onion_skin.rst:41
msgid ""
"The :guilabel:`Previous Frame` and :guilabel:`Next Frame` color labels "
"allows you set the colors."
msgstr ""
"De kleurlabels :guilabel:`Vorige frame` en :guilabel:`Volgende frame` bieden "
"u het instellen van de kleuren."
