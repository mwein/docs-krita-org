# Translation of docs_krita_org_reference_manual___tools___path_select.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_reference_manual___tools___path_select\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-02 11:36+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../<generated>:1
msgid "Activate Angle Snap"
msgstr "Задіяння прилипання під кутом"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"

#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"

#: ../../<rst_epilog>:72
msgid ""
".. image:: images/icons/path_select_tool.svg\n"
"   :alt: toolselectpath"
msgstr ""
".. image:: images/icons/path_select_tool.svg\n"
"   :alt: toolselectpath"

#: ../../reference_manual/tools/path_select.rst:1
msgid "Krita's bezier curve selection tool reference."
msgstr "Довідник із інструмента позначення кривою Безьє Krita."

#: ../../reference_manual/tools/path_select.rst:11
msgid "Tools"
msgstr "Інструменти"

#: ../../reference_manual/tools/path_select.rst:11
msgid "Vector"
msgstr "Вектор"

#: ../../reference_manual/tools/path_select.rst:11
msgid "Path"
msgstr "Контур"

#: ../../reference_manual/tools/path_select.rst:11
msgid "Bezier Curve"
msgstr "Крива Безьє"

#: ../../reference_manual/tools/path_select.rst:11
msgid "Pen"
msgstr "Перо"

#: ../../reference_manual/tools/path_select.rst:11
msgid "Selection"
msgstr "Позначення"

#: ../../reference_manual/tools/path_select.rst:17
msgid "Path Selection Tool"
msgstr "Інструмент «Позначення контуру»"

#: ../../reference_manual/tools/path_select.rst:19
msgid "|toolselectpath|"
msgstr "|toolselectpath|"

#: ../../reference_manual/tools/path_select.rst:21
msgid ""
"This tool, represented by an ellipse with a dashed border and a curve "
"control, allows you to make a :ref:`selections_basics` of an area by drawing "
"a path around it. Click where you want each point of the path to be. Click "
"and drag to curve the line between points. Finally click on the first point "
"you created to close your path."
msgstr ""
"За допомогою цього інструмента, на кнопці якого показано частину еліпса із "
"пунктирною межею та елемент керування кривою, ви можете створювати :ref:"
"`selections_basics` ділянки, намалювавши контур навколо неї. Клацніть там, "
"де хочете розташувати опорні точки контуру. Натисніть ліву кнопку миші і "
"перетягніть вказівник, щоб створити криву між двома точками. Нарешті, "
"клацніть на першій створеній точці, щоб замкнути контур."

#: ../../reference_manual/tools/path_select.rst:24
msgid "Hotkeys and Sticky keys"
msgstr "Керування за допомогою клавіатури"

#: ../../reference_manual/tools/path_select.rst:26
msgid ""
":kbd:`R` sets the selection to 'replace' in the tool options, this is the "
"default mode."
msgstr ""
":kbd:`R` встановлює параметри позначення у режим «замінити»; це типовий "
"режим."

#: ../../reference_manual/tools/path_select.rst:27
msgid ":kbd:`A` sets the selection to 'add' in the tool options."
msgstr ""
":kbd:`A` встановлює для позначення режим «додавання» у параметрах "
"інструмента."

#: ../../reference_manual/tools/path_select.rst:28
msgid ":kbd:`S` sets the selection to 'subtract' in the tool options."
msgstr ""
":kbd:`S` встановлює для позначення режим «віднімання» у параметрах "
"інструмента."

#: ../../reference_manual/tools/path_select.rst:29
msgid ""
":kbd:`Shift +` |mouseleft| sets the subsequent selection to 'add'. You can "
"release the :kbd:`Shift` key while dragging, but it will still be set to "
"'add'. Same for the others."
msgstr ""
"За допомогою комбінації :kbd:`Shift` + |mouseleft| можна встановити для "
"наступного позначення області режим «додавання». Під час перетягування ви "
"можете відпустити клавішу :kbd:`Shift`, але режим лишатиметься режимом "
"«додавання». Те саме стосується і інших режимів."

#: ../../reference_manual/tools/path_select.rst:30
msgid ":kbd:`Alt +` |mouseleft| sets the subsequent selection to 'subtract'."
msgstr ""
":kbd:`Alt` + |mouseleft| встановлює для наступного позначення режим "
"«віднімання»."

#: ../../reference_manual/tools/path_select.rst:31
msgid ":kbd:`Ctrl +` |mouseleft| sets the subsequent selection to 'replace'."
msgstr ""
":kbd:`Ctrl` + |mouseleft| встановлює для наступного позначення режим "
"«заміна»."

#: ../../reference_manual/tools/path_select.rst:32
msgid ""
":kbd:`Shift + Alt +` |mouseleft| sets the subsequent selection to "
"'intersect'."
msgstr ""
":kbd:`Shift + Alt +` |mouseleft| встановлює для наступного позначення режим "
"«перетин»."

#: ../../reference_manual/tools/path_select.rst:37
msgid "Hovering over a selection allows you to move it."
msgstr ""
"Наведення вказівника на позначену ділянку надасть вам змогу пересунути її."

#: ../../reference_manual/tools/path_select.rst:38
msgid ""
"|mouseright| will open up a selection quick menu with amongst others the "
"ability to edit the selection."
msgstr ""
"Клацання |mouseright| відкриває меню швидкого доступу для позначеної "
"ділянки, у якому, серед інших пунктів, є і пункт редагування позначеної "
"ділянки."

#: ../../reference_manual/tools/path_select.rst:43
msgid ""
"You can switch the behavior of the :kbd:`Alt` key to use the :kbd:`Ctrl` key "
"instead by toggling the switch in the :ref:`general_settings`."
msgstr ""
"Ви можете перемкнути поведінку програми у відповідь на натискання :kbd:`Alt` "
"так, щоб натискати замість цієї клавіші клавішу :kbd:`Ctrl`. Для цього "
"призначено спеціальний пункт на сторінці :ref:`general_settings`."

#: ../../reference_manual/tools/path_select.rst:46
msgid "Tool Options"
msgstr "Параметри інструмента"

#: ../../reference_manual/tools/path_select.rst:50
#: ../../reference_manual/tools/path_select.rst:58
msgid "Autosmooth Curve"
msgstr "Автозгладжування кривої"

#: ../../reference_manual/tools/path_select.rst:51
#: ../../reference_manual/tools/path_select.rst:59
msgid ""
"Toggling this will have nodes initialize with smooth curves instead of "
"angles. Untoggle this if you want to create sharp angles for a node. This "
"will not affect curve sharpness from dragging after clicking."
msgstr ""
"Позначення цього пункту призведе до того, що вузли контуру створюватимуться "
"як точки гладкої кривої, а не кутові точки. Зніміть позначення з цього "
"пункту, якщо хочете створювати гострі кути у вузлах контуру. Позначення чи "
"непозначення цього пункту не впливатиме на різкість сегмента кривої, який "
"утворюватиметься у результаті перетягування вказівника після натискання "
"лівої кнопки миші."

#: ../../reference_manual/tools/path_select.rst:54
msgid "Anti-aliasing"
msgstr "Згладжування"

#: ../../reference_manual/tools/path_select.rst:54
msgid ""
"This toggles whether or not to give selections feathered edges. Some people "
"prefer hard-jagged edges for their selections."
msgstr ""
"Цей пункт визначає, чи буде програма згладжувати межі позначеної ділянки. "
"Дехто надає перевагу різким межам позначених ділянок."

#: ../../reference_manual/tools/path_select.rst:61
msgid "Angle Snapping Delta"
msgstr "Крок кута прилипання"

#: ../../reference_manual/tools/path_select.rst:62
msgid "The angle to snap to."
msgstr "Кут для прилипання."

#: ../../reference_manual/tools/path_select.rst:64
msgid ""
"Angle snap will make it easier to have the next line be at a specific angle "
"of the current. The angle is determined by the :guilabel:`Angle Snapping "
"Delta`."
msgstr ""
"Прилипання за кутом спрощує створення кривих, у яких кожен наступний сегмент "
"буде розташовано під певним кутом до поточного. Кут визначається параметром :"
"guilabel:`Крок кута прилипання`."

#~ msgid ""
#~ "Selection modifiers don't quite work yet with the path tool, as :kbd:"
#~ "`Shift` breaks the path"
#~ msgstr ""
#~ "Модифікатори позначення для інструмента вибору кольором не працюють як "
#~ "слід, оскільки :kbd:`Shift` призводить до розривання контуру."
